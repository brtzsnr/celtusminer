#!/bin/sh

set -e  # exit on error
set -x  # echo

cd `dirname $0`
export GOPATH=`pwd`/..
go test celtus
version=`git describe --tags`
go build -ldflags "-X main.version '$version'" celtus
