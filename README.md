# celtusminer: A Primecoin pool miner

[Website](https://github.com/brtzsnr/celtusminer) |
[Wiki](https://github.com/brtzsnr/celtusminer/wiki) |
[Mailing List](https://groups.google.com/forum/#!forum/celtusminer) |
[Peercoin Thread](http://www.peercointalk.org/index.php?topic=3252.msg30831#msg30831)

[![Build Status](https://travis-ci.org/brtzsnr/celtusminer.svg?branch=master)](https://travis-ci.org/brtzsnr/celtusminer)

celtusminer is a [Primecoin](http://primecoin.org) pool miner.
celtusminer supports the same protocol as [xolominer](https://github.com/thbaumbach/primecoin)
so it can mine primecoins for pools such as http://rpool.net and http://beeeeer.org.

Currently, celtusminer is 1-2 orders of magnitude slower than state-of-the-art
primecoin CPU miners. Do not use it for serious mining just yet.

## Usage

```
#!bash

$ go version
go version go1.3 linux/amd64
$ git clone https://github.com/brtzsnr/celtusminer
$ cd celtusminer
$ git checkout v0.1.0
$ bin/build.sh
$ bin/celtus -help
$ bin/celtus -port 10000 -pool xolo://Aczm1s7f6dV1GuMDbjR48LkMnD2nscoEtk@176.34.128.129:1337
13:22:11 main.go:80: Started celtusminer aa854bb7aa09+ 76+ tip
13:22:11 main.go:85: Debugging address localhost:10000
13:22:11 main.go:118: Connected to 176.34.128.129:1337
13:22:11 xolo.go:112: Logged in as Aczm1s7f6dV1GuMDbjR48LkMnD2nscoEtk
13:22:11 xolo.go:157: Got work, target = 0a.f6d0b0
```

## Roadmap

1. soon: Tweak and improve mining algorithm.
1. soon: Add support for http://ypool.net and maybe other protocols.
1. future: Add support for related coins, such as [Riecoin](http://riecoin.org/).


## Implementation details

Celtus is written in [Go](http://golang.org). This means that it performs worse
than other highly optimized implementations such as xolominer or jhprimeminer.
The goal of the project is to explore different algorithms for prime chain finding.

The current mining algorithm is based on the understanding of the
original problem presented in Primecoin white-paper. It has the same principle
as the original mining algorithm:

* sieving removes composites that have small factors
* pseudo-primality testing of chains that passed sieving


## Development

Patches are welcomed. Patches to the Go compilers are encouraged.
When sending pull requests keep in mind the following:

1. Your code will be reviewed. Expect that I will reject some patches.
1. Keep the code simple. I prefer simpler code over complex abstractions.
This will allow the code to evolve nicely over time.
1. Benchmark before doing performance optimizations. Make sure
you optimize the right things.
1. `go fmt`, `go vet`, `go test`, `go test -cover` are your friends.


## If you like celtusminer you may send some coins to

XPM: Aczm1s7f6dV1GuMDbjR48LkMnD2nscoEtk


## Disclaimer

celtusprimer project is not affiliated with my employer.
