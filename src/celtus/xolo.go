// xolo implements xolominer protocol.
//
// Protocol has been reverse engineered from:
// https://github.com/thbaumbach/primecoin/blob/master/src/main_poolminer.cpp
package main

import (
	"bytes"
	"crypto/sha1"
	"crypto/sha256"
	"encoding/binary"
	"expvar"
	"fmt"
	"log"
	"net"
)

const (
	xcVersionMajor uint8 = 1
	xcVersionMinor uint8 = 1
)

var (
	shares = expvar.NewMap("Shares")
)

type blockRaw struct {
	Version         uint32
	HashPrevBlock   [sha256.Size]byte
	HashMerkleRoot  [sha256.Size]byte
	Timestamp       uint32
	Bits            uint32
	Nonce           uint32
	PrimeMultiplier [48]byte
}

type XoloClient struct {
	Username string
	Password string
	Server   string

	// Connection to the mining pool.
	conn *net.TCPConn
}

// Connect connects to xc.Server mining pool.
func (xc *XoloClient) Connect() error {
	var err error
	addr, err := net.ResolveTCPAddr("tcp", xc.Server)
	if err != nil {
		return err
	}

	xc.conn, err = net.DialTCP("tcp", nil, addr)
	return err
}

// Close closses the tcp connection to the pool.
func (xc *XoloClient) Close() error {
	return xc.conn.Close()
}

func hashPassword(p string) string {
	b := sha1.Sum([]byte(p))
	var s string
	for i := 0; i < 4; i++ {
		s += fmt.Sprintf("%02x", b[0*4+i]^b[1*4+i]^b[4*4+i])
	}
	for i := 0; i < 4; i++ {
		s += fmt.Sprintf("%02x", b[2*4+i]^b[3*4+i]^b[4*4+i])
	}
	return s
}

// Login logins to xolo server.
func (xc *XoloClient) Login() error {
	buf := &bytes.Buffer{}

	binary.Write(buf, binary.LittleEndian, uint8(len(xc.Username)))
	buf.WriteString(xc.Username)

	type login struct {
		Zero             uint8
		VersionMajor     uint8
		VersionMinor     uint8
		ThreadNumMax     uint8
		PoolFeePercent   uint8
		MinerId          uint16
		NSieveExtensions uint32 // TODO
		NSievePercentage uint32 // TODO
		NSieveSize       uint32 // TODO
	}

	l := login{}
	l.VersionMajor = xcVersionMajor
	l.VersionMinor = xcVersionMinor
	l.ThreadNumMax = 1
	l.PoolFeePercent = 2
	l.MinerId = 0
	binary.Write(buf, binary.LittleEndian, l)

	password := hashPassword(xc.Password)
	binary.Write(buf, binary.LittleEndian, uint8(len(password)))
	buf.WriteString(password)

	// Write developer fee id
	binary.Write(buf, binary.LittleEndian, uint16(0))

	if _, err := xc.conn.Write(buf.Bytes()); err != nil {
		return err
	}
	log.Println("Logged in as", xc.Username)
	return nil
}

func (xc *XoloClient) Process() (*BlockHeader, error) {
	var type_ uint8
	if err := binary.Read(xc.conn, binary.LittleEndian, &type_); err != nil {
		return nil, err
	}

	var dataLen int
	switch type_ {
	case 0:
		dataLen = 128
	case 1:
		dataLen = 4
	case 2:
		// Ping-Pong.
		return nil, nil
	default:
		return nil, fmt.Errorf("Unknown type %d", type_)
	}

	data := make([]byte, dataLen)
	if n, err := xc.conn.Read(data); err != nil {
		return nil, err
	} else if n != dataLen {
		return nil, fmt.Errorf("expected %d bytes, got %d", dataLen, n)
	}

	buf := bytes.NewReader(data)
	if type_ == 0 {
		// New work.
		b := blockRaw{}
		if err := binary.Read(buf, binary.LittleEndian, &b); err != nil {
			return nil, err
		}

		bh := &BlockHeader{}
		bh.Version = b.Version
		bh.HashPrevBlock = b.HashPrevBlock
		bh.HashMerkleRoot = b.HashMerkleRoot
		bh.Timestamp = b.Timestamp
		bh.Bits = b.Bits
		bh.Nonce = b.Nonce
		log.Printf("Got work, target = %02x.%06x", bh.Bits>>24, bh.Bits&0xffffff)
		return bh, nil
	}

	if type_ == 1 {
		// Share info.
		var retVal int32
		if err := binary.Read(buf, binary.LittleEndian, &retVal); err != nil {
			return nil, err
		}

		result := ""
		if retVal == 0 {
			result = "Rejected"
		} else if retVal < 0 {
			result = "Stale"
		} else if retVal > 100000 {
			result = "Block"
		} else {
			result = "Share"
		}

		log.Println("[Master] submitted share ->", result)
		shares.Add(result, 1)
		return nil, nil
	}

	log.Printf("got type_ %d", type_)
	log.Printf("read %d bytes: %x", dataLen, data)
	return nil, nil
}

// blockHeaderToRaw convers a BlockHeader to blockRaw.
func blockHeaderToRaw(pch *PrimecoinHeader) (*blockRaw, error) {
	primeMultiplier := pch.ChainMultiplier.Bytes()
	reverse(primeMultiplier)

	b := &blockRaw{}
	b.Version = pch.Version
	b.HashPrevBlock = pch.HashPrevBlock
	b.HashMerkleRoot = pch.HashMerkleRoot
	b.Timestamp = pch.Timestamp
	b.Bits = pch.Bits
	b.Nonce = pch.Nonce

	b.PrimeMultiplier[0] = uint8(len(primeMultiplier))
	n := copy(b.PrimeMultiplier[1:], primeMultiplier)
	if n != len(primeMultiplier) {
		return nil, fmt.Errorf("prime multiplier is too large, %d > %d",
			len(primeMultiplier), len(b.PrimeMultiplier[:]))
	}
	return b, nil
}

// SubmitWork submits proof-of-work.
func (xc *XoloClient) SubmitWork(pch *PrimecoinHeader) error {
	b, err := blockHeaderToRaw(pch)
	if err != nil {
		return err
	}
	buf := &bytes.Buffer{}
	binary.Write(buf, binary.LittleEndian, *b)
	if _, err := xc.conn.Write(buf.Bytes()); err != nil {
		return err
	}

	shares.Add("Submitted", 1)
	log.Printf("Submitted %v chain of length %d", pch.ChainType, pch.ChainLength)
	return nil
}
