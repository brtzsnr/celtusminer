package main

import (
	"testing"
)

var (
	pch333211 = &PrimecoinHeader{
		BlockHeader: BlockHeader{
			Version:        2,
			HashPrevBlock:  hexToHash("052bfa144156c1ae1465ba61bf68e74aec35dcbec4ed1afffad6deb889f77c22"),
			HashMerkleRoot: hexToHash("a723036351bf02fc533fe197da1baad2e97566ff417f023d932378a747831c32"),
			Timestamp:      0x52bee89c,
			Bits:           0x0a2a1294,
			Nonce:          0x0025578a,
		},
		ChainType:       ChainCunningham1,
		ChainLength:     9,
		ChainMultiplier: mpiToBig([]byte{0x0c, 0x00, 0x00, 0x00, 0x97, 0x58, 0xd6, 0x93, 0x7a, 0x93, 0x6e, 0xbf, 0x03}),
	}

	pch200000 = &PrimecoinHeader{
		BlockHeader: BlockHeader{
			Version:        2,
			HashPrevBlock:  hexToHash("1bdba54c7afd7706d70b228edd8379f3712c1bd1575bafbf631e6578a472f25f"),
			HashMerkleRoot: hexToHash("4876c82c7bba84f211a6f652a05a7446332646ea560f3ae58ae5a1c405167077"),
			Timestamp:      0x52544812,
			Bits:           0x09e36ecf,
			Nonce:          0x00001df7,
		},
		ChainType:       ChainBitwin,
		ChainLength:     9,
		ChainMultiplier: mpiToBig([]byte{0x08, 0x60, 0xde, 0xbb, 0x60, 0xbc, 0x44, 0x96, 0x0d}),
	}
)

func TestBlockHash(t *testing.T) {
	hash := hashToHex(pch200000.BlockHash())
	expected := "c4cb2d790a4edf8a9e11615b22b1c45eb0688c793a1d07a6464704750f6d9448"
	if hash != expected {
		t.Errorf("Expected\n  %s\ngot\n  %s", expected, hash)
	}

	hash = hashToHex(pch333211.BlockHash())
	expected = "9d706c9531acb5b69b63712001e8ea65275fdc6b111c8cb6aaeaa7d701274f78"
	if hash != expected {
		t.Errorf("Expected\n  %s\ngot\n  %s", expected, hash)
	}
}

func TestPrimecoinHash(t *testing.T) {
	hash := hashToHex(pch200000.PrimecoinHash())
	expected := "4669276a932f5b16a718943a4930b5381bf9c7780ed45b2a751171d3feda174c"
	if hash != expected {
		t.Errorf("Expected\n  %s\ngot\n  %s", expected, hash)
	}

	hash = hashToHex(pch333211.PrimecoinHash())
	expected = "89f721b8b8fbfc225b62bac1520d15808cd3399c19d81c2827a763b6637e617b"
	if hash != expected {
		t.Errorf("Expected\n  %s\ngot\n  %s", expected, hash)
	}
}

func testPrimeChain(t testing.TB, pch *PrimecoinHeader) {
	if pch.ChainMultiplier.Sign() <= 0 {
		t.Fatal("Expected positive ChainMultiplier, got ", pch.ChainMultiplier)
	}
	if err := pch.VerifyChain(); err != nil {
		t.Fatal("Bad chain: ", err)
	}
	if headerHashMinimum.Cmp(hashToBig(pch.BlockHash())) > 0 {
		t.Fatal("Block hash must have MSB set to 1")
	}
}

func TestPrimeChains(t *testing.T) {
	testPrimeChain(t, pch200000)
	testPrimeChain(t, pch333211)
}

func BenchmarkPrimeChains(b *testing.B) {
	for i := 0; i < b.N; i++ {
		testPrimeChain(b, pch200000)
		testPrimeChain(b, pch333211)
	}
}

func TestSearchChainMultiplier2(t *testing.T) {
	celtus := &Celtus{Block: pch200000.BlockHeader}
	celtus.ChooseNonce()
	celtus.PrepareSearch()
	for i := 0; i < 15; i++ {
		pch := celtus.SearchChainMultiplier(2, make(chan struct{}))
		testPrimeChain(t, pch)
	}
}

func TestSearchChainMultiplier3(t *testing.T) {
	celtus := &Celtus{Block: pch333211.BlockHeader}
	celtus.ChooseNonce()
	celtus.PrepareSearch()
	for i := 0; i < 5; i++ {
		pch := celtus.SearchChainMultiplier(3, make(chan struct{}))
		testPrimeChain(t, pch)
	}
}

func benchmarkSearchChainMultiplier(b *testing.B, chainLength uint) {
	for _, bh := range []BlockHeader{pch200000.BlockHeader, pch333211.BlockHeader} {
		celtus := &Celtus{Block: bh}
		celtus.PrepareSearch()
		for i := 0; i < b.N; i++ {
			celtus.SearchChainMultiplier(chainLength, make(chan struct{}))
		}
	}
}

func BenchmarkSearchChainMultiplier2(b *testing.B) {
	benchmarkSearchChainMultiplier(b, 2)
}

func BenchmarkSearchChainMultiplier3(b *testing.B) {
	benchmarkSearchChainMultiplier(b, 3)
}

func BenchmarkSearchChainMultiplier4(b *testing.B) {
	benchmarkSearchChainMultiplier(b, 4)
}

func BenchmarkSearchChainMultiplier5(b *testing.B) {
	benchmarkSearchChainMultiplier(b, 5)
}

func BenchmarkSearchChainMultiplier6(b *testing.B) {
	benchmarkSearchChainMultiplier(b, 6)
}
