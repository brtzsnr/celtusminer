package main

import (
	"encoding/hex"
	"math/big"
)

func reverse(buf []byte) {
	for i := 0; i < len(buf)/2; i++ {
		j := len(buf) - 1 - i
		buf[i], buf[j] = buf[j], buf[i]
	}
}

func hexToHash(str string) [32]byte {
	buf, _ := hex.DecodeString(str)
	reverse(buf)

	var tmp [32]byte
	copy(tmp[:], buf)
	return tmp
}

func hashToHex(buf []byte) string {
	tmp := make([]byte, len(buf))
	copy(tmp, buf)
	reverse(tmp)
	return hex.EncodeToString(tmp)
}

// bigToMPI converts a big integer to MPI representation.
func bigToMpi(b *big.Int) []byte {
	buf := b.Bytes()
	for len(buf)%4 != 0 {
		buf = append(buf, 0x00)
	}

	buf = append(buf, byte(len(buf)))
	reverse(buf)
	return buf
}

// mpiToBig converts a big integer from MPI representation.
func mpiToBig(buf []byte) *big.Int {
	bufCopy := make([]byte, len(buf))
	copy(bufCopy, buf)
	b := big.NewInt(0)
	reverse(bufCopy[1:])
	b.SetBytes(bufCopy[1:])
	return b
}

// hashToBig converts a hash number to an integer
func hashToBig(hash []byte) *big.Int {
	tmp := make([]byte, len(hash))
	copy(tmp, hash)
	reverse(tmp)
	return new(big.Int).SetBytes(tmp)
}
