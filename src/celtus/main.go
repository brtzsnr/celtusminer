package main

import (
	"expvar"
	"flag"
	"fmt"
	"log"
	"net/http"
	_ "net/http/pprof"
	"net/url"
	"os"
)

var (
	pool           = flag.String("pool", "", "Pool info as URI reference. Use xolo://username:password@host (required)")
	port           = flag.Int("port", 0, "debugging port")
	minChainLength = flag.Int("min_chain_length", 7, "minimum chain length to submit")

	numBlocks      = expvar.NewInt("NumBlocks")
	numSearches    = expvar.NewInt("NumSearches")
	hashPrevBlock  = expvar.NewString("HashPrevBlock")
	hashMerkleRoot = expvar.NewString("HashMerkleRoot")

	version string = "devel"
)

func init() {
	log.SetFlags(log.Ltime | log.Lshortfile)
	flag.Parse()
}

func search(xc *XoloClient, bh *BlockHeader, done <-chan struct{}) {
	celtus := &Celtus{Block: *bh}
	if err := celtus.ChooseNonce(); err != nil {
		log.Printf("Aborting search: %v", err)
		return
	}

	celtus.PrepareSearch()
	for {
		// Check if computation needs to stop.
		select {
		case <-done:
			return
		default:
		}

		numSearches.Add(1)
		pch := celtus.SearchChainMultiplier(uint(*minChainLength), done)
		if pch == nil {
			continue
		}
		if err := pch.VerifyChain(); err != nil {
			log.Printf("%v chain of length %d: %v", pch.ChainType, pch.ChainLength, err)
			continue
		}

		log.Printf("%v chain of length %d: passed StrongPseudoPrime", pch.ChainType, pch.ChainLength)
		log.Printf("PrimecoinHash: %s", hashToHex(pch.PrimecoinHash()))
		log.Printf("    BlockHash: %s", hashToHex(pch.BlockHash()))

		// TODO: Submit work asynchronously.
		if err := xc.SubmitWork(pch); err != nil {
			log.Println("failed to submit share: ", err)
		}
	}
}

func main() {
	if *pool == "" {
		fmt.Fprintf(os.Stderr, "Please provide a pool URI using --pool flag\n")
		fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
		fmt.Fprintf(os.Stderr, "\n")
		fmt.Fprintf(os.Stderr, "Example:\n")
		fmt.Fprintf(os.Stderr, "\t%s -port 10000 -pool xolo://user:pass@hostport\n", os.Args[0])
		return
	}

	log.Printf("Started celtusminer %v", version)

	// Start http server for debugging and profiling.
	if *port != 0 {
		address := fmt.Sprintf("localhost:%d", *port)
		log.Println("Debugging address", address)
		go http.ListenAndServe(address, nil)
	}

	// Parse pool information.
	url, err := url.Parse(*pool)
	if err != nil {
		log.Fatal("Cannot parse pool: ", err)
	}
	if url.Scheme != "xolo" {
		log.Fatal("Invalid pool. Scheme must be xolo, got ", url.Scheme)
	}
	if url.User == nil {
		log.Fatal("User info not specified")
	}
	username := url.User.Username()
	password, ok := url.User.Password()
	if !ok || password == "" {
		// Provide a default password if it is missing.
		// Some pools accept the XPM address as user, no password required.
		password = "uglyuglyugly"
	}

	// Connect to pool
	xc := &XoloClient{
		Username: username,
		Password: password,
		Server:   url.Host,
	}
	if err := xc.Connect(); err != nil {
		log.Fatalln("Cannot connect to", xc.Server, ":", err)
	}
	defer xc.Close()
	log.Println("Connected to", xc.Server)

	// TODO: Loop if client gets disconnected.
	if err := xc.Login(); err != nil {
		log.Fatalln("Cannot log in as", xc.Username, ":", err)
	}

	done := make(chan struct{})
	for {
		bh, err := xc.Process()
		if err != nil {
			log.Fatalln("Cannot process :", err)
		}

		if bh != nil {
			numBlocks.Add(1)
			hashPrevBlock.Set(fmt.Sprintf("% x", bh.HashPrevBlock[:]))
			hashMerkleRoot.Set(fmt.Sprintf("% x", bh.HashMerkleRoot[:]))

			close(done)
			done = make(chan struct{})
			go search(xc, bh, done)
		}
	}
}
