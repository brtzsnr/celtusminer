// coin.go implements coin handling functionality.
package main

import (
	"crypto/sha256"
	"encoding/binary"
	"fmt"
	"math/big"
)

const (
	HashSize = sha256.Size
)

// Possible chain types.
type ChainType int

const (
	ChainInvalid ChainType = iota
	ChainCunningham1
	ChainCunningham2
	ChainBitwin
)

func (ct ChainType) String() string {
	switch ct {
	case ChainCunningham1:
		return "Cunningham1"
	case ChainCunningham2:
		return "Cunningham2"
	case ChainBitwin:
		return "Bitwin"
	default:
		return "(invalid)"
	}
}

// Standard Bitcoin header.
type BlockHeader struct {
	Version        uint32
	HashPrevBlock  [HashSize]byte
	HashMerkleRoot [HashSize]byte
	Timestamp      uint32
	Bits           uint32
	Nonce          uint32
}

// BlockHash calculates the block header hash.
// This is the fixed multiplier when searching for primes.
// NOTE: When converting to an Int the hash must be reversed.
func (bh *BlockHeader) BlockHash() []byte {
	hash := sha256.New()
	binary.Write(hash, binary.LittleEndian, bh.Version)
	hash.Write(bh.HashPrevBlock[:])
	hash.Write(bh.HashMerkleRoot[:])
	binary.Write(hash, binary.LittleEndian, bh.Timestamp)
	binary.Write(hash, binary.LittleEndian, bh.Bits)
	binary.Write(hash, binary.LittleEndian, bh.Nonce)

	data := sha256.Sum256(hash.Sum(nil))
	return data[:]
}

// Primecoin header.
type PrimecoinHeader struct {
	BlockHeader

	ChainType       ChainType
	ChainLength     int
	ChainMultiplier *big.Int
}

// PrimecoinHash calculates the hash.
// Same as BlockHash, but includes ChainMultiplier.
func (pch *PrimecoinHeader) PrimecoinHash() []byte {
	hash := sha256.New()
	binary.Write(hash, binary.LittleEndian, pch.Version)
	hash.Write(pch.HashPrevBlock[:])
	hash.Write(pch.HashMerkleRoot[:])
	binary.Write(hash, binary.LittleEndian, pch.Timestamp)
	binary.Write(hash, binary.LittleEndian, pch.Bits)
	binary.Write(hash, binary.LittleEndian, pch.Nonce)
	hash.Write(bigToMpi(pch.ChainMultiplier))

	data := sha256.Sum256(hash.Sum(nil))
	return data[:]
}

// Origin returns the origin of the current chain.
func (pch *PrimecoinHeader) Origin() *big.Int {
	if pch.ChainMultiplier == nil {
		return nil
	}
	origin := hashToBig(pch.BlockHash())
	return origin.Mul(origin, pch.ChainMultiplier)
}

// VerifyChain verifies the primality of the elements in the chain.
func (pch *PrimecoinHeader) VerifyChain() error {
	origin := pch.Origin()
	var delta []*big.Int

	if pch.ChainType == ChainCunningham1 {
		delta = []*big.Int{minusOne, minusOne}
	} else if pch.ChainType == ChainCunningham2 {
		delta = []*big.Int{plusOne, plusOne}
	} else if pch.ChainType == ChainBitwin {
		delta = []*big.Int{minusOne, plusOne}
	} else {
		return fmt.Errorf("invalid chain type")
	}

	for i := 0; i < pch.ChainLength; i++ {
		origin.Add(origin, delta[i%2])
		if err := StrongPseudoPrime(origin); err != nil {
			return fmt.Errorf("%v failed at %d: %v", pch.ChainType, i, err)
		}
		origin.Sub(origin, delta[i%2])
		if pch.ChainType != ChainBitwin || i%2 == 1 {
			origin.Lsh(origin, 1)
		}
	}

	return nil
}
