package main

import (
	"expvar"
	"flag"
	"fmt"
	"log"
	"math"
	"math/big"
)

var (
	sieveSize      = flag.Int("sieve_size", 1000000, "sieve size")
	coprimeFactors = flag.Int("coprime_factors", 10, "number of fixed multiplier coprime factors")

	numShift      = expvar.NewMap("NumShift")
	numCC1        = expvar.NewMap("NumCC1")
	numCC2        = expvar.NewMap("NumCC2")
	numBTW        = expvar.NewMap("NumBTW")
	numCandidates = expvar.NewMap("NumCandidates")

	minusOne = big.NewInt(-1)
	plusOne  = big.NewInt(1)
	plusTwo  = big.NewInt(2)

	headerHashMinimum = new(big.Int).Lsh(big.NewInt(1), 255)
)

type Celtus struct {
	Block BlockHeader

	// A factor used to eliminate a few small primes from search.
	coprimeMultiplier *big.Int
	// Fixed multiplier. Origin will be a factor of this.
	// fixedMultiplier = hashToBig(HeaderHash) * coprimeMultiplier
	fixedMultiplier *big.Int
	// fixedMultiplier modulo some small prime numbers
	modulos []Modulo
	// Real chainMultiplier is the product of these chainMultiplier and coprimeMultiplier.
	chainMultiplier int64
	// Current tested origin.
	// origin = hashToBig(HeaderHash) * coprimeMultiplier * chainMultiplier
	origin *big.Int

	// Mark the end of the range for sieves.
	chainMultiplierLimit int64
	// Sieve for Cunningham Chain type 1.
	sieve1 *Sieve
	// Sieve for Cunningham Chain type 2.
	sieve2 *Sieve
}

// primeTester performs Fermat Pseudo Primality tests, caching results.
type primeTester struct {
	Origin        *big.Int
	ProbablePrime uint32
	ChainType     ChainType

	delta     *big.Int
	lazyShift uint
	fermat    uint32
}

func newPrimeTester(origin *big.Int, type_ ChainType, probablePrime uint32) *primeTester {
	var delta *big.Int
	if type_ == ChainCunningham1 {
		delta = minusOne
	} else if type_ == ChainCunningham2 {
		delta = plusOne
	} else {
		panic("invalid chain type")
	}

	return &primeTester{
		Origin:        new(big.Int).Set(origin),
		ProbablePrime: probablePrime,
		ChainType:     type_,
		delta:         delta,
	}
}

// shift sets origin at original origin * 2^s
func (pt *primeTester) shift(s uint) {
	if s < pt.lazyShift {
		pt.Origin.Rsh(pt.Origin, pt.lazyShift-s)
	} else if s > pt.lazyShift {
		pt.Origin.Lsh(pt.Origin, s-pt.lazyShift)
	}
	pt.lazyShift = s
}

// fermatStats stores some stats regarding Fermat Pseudo Primality testing.
// [shift][0] - num composite
// [shift][1] - num tests
var fermatStats [32][2]int

// IsPseudoPrime test primality of origin * 2^shift + delta.
func (pt *primeTester) IsPseudoPrime(shift uint) bool {
	bit := uint32(1) << shift
	if pt.ProbablePrime&bit == 0 {
		return false
	}
	if pt.fermat&bit != 0 {
		return true
	}

	pt.shift(shift)
	pt.Origin.Add(pt.Origin, pt.delta)
	pseudoPrime := FermatPseudoPrime(pt.Origin, plusTwo)
	pt.Origin.Sub(pt.Origin, pt.delta)

	pt.fermat |= bit
	fermatStats[shift][1]++
	if !pseudoPrime {
		pt.ProbablePrime &= ^bit
		fermatStats[shift][0]++
	}

	return pseudoPrime
}

// findCunninghamChain looks for a Cunningham chain at origin pt.Origin of
// minimum length minChainLength.
// Returns pair (chain length, origin shift).
func findCunninghamChain(minChainLength uint, pt *primeTester) (uint, uint) {
	mask := uint32(1<<minChainLength - 1)
ShiftLoop:
	for s := uint(0); s < 32; s++ {
		// Test for chainLength consecutive possible primes.
		if (pt.ProbablePrime>>s)&mask != mask {
			continue
		}
		numCandidates.Add(fmt.Sprint(pt.ChainType), 1)

		// Test in reverse order the first minChainLength elements of the chain.
		// Assuming that all chain elements have the same chance to be prime then
		// knowing that higher elements are composite is going to exclude larger
		// shifts (s).
		for l := minChainLength; l > 0; l-- {
			if !pt.IsPseudoPrime(s + l - 1) {
				continue ShiftLoop
			}
		}

		// Test for a Cunningham chain at origin * 2^s of length e-s.
		e := s
		for ; e < 32 && pt.IsPseudoPrime(e); e++ {
		}
		if e-s >= minChainLength {
			return e - s, s
		}
	}

	return 0, 0
}

// findBitwinChain looks for a Bitwin chain at origin cc1pt.Origin of
// minimum length minChainLength.
// Returns pair (chain length, origin shift).
func findBitwinChain(minChainLength uint, cc1pt, cc2pt *primeTester) (uint, uint) {
	cc1Mask := uint32(1)<<(minChainLength/2+minChainLength%2) - 1
	cc2Mask := uint32(1)<<(minChainLength/2) - 1

ShiftLoop:
	for s := uint(0); s < 32; s++ {
		// Test for Cunningham chains of sufficient length.
		if (cc1pt.ProbablePrime>>s)&cc1Mask != cc1Mask {
			continue
		}
		if (cc2pt.ProbablePrime>>s)&cc2Mask != cc2Mask {
			continue
		}
		numCandidates.Add(fmt.Sprint(ChainBitwin), 1)

		for l := minChainLength/2 + minChainLength%2; l > 0; l-- {
			if !cc1pt.IsPseudoPrime(s + l - 1) {
				continue ShiftLoop
			}
		}
		for l := minChainLength / 2; l > 0; l-- {
			if !cc2pt.IsPseudoPrime(s + l - 1) {
				continue ShiftLoop
			}
		}

		// Test for a Bitwin chain at origin * 2^s of length l.
		l := uint(0)
		for ; l/2+s < 32; l++ {
			if l%2 == 0 && !cc1pt.IsPseudoPrime(s+l/2) {
				break
			}
			if l%2 == 1 && !cc2pt.IsPseudoPrime(s+l/2) {
				break
			}
		}
		if l >= minChainLength {
			return l, s
		}
	}

	return 0, 0
}

// computeCoprimeMultiplier finds smallest m, coprime with n, that has
// numFactors prime factors.
func computeCoprimeMultiplier(n *big.Int, numFactors int) *big.Int {
	k := 0
	multiplier := big.NewInt(1)
	for _, divisor := range SmallPrimes {
		remainder := BigMod(n, divisor)
		if remainder != 0 {
			multiplier.Mul(multiplier, big.NewInt(divisor))
			k++
			if k == numFactors {
				break
			}
		}
	}
	return multiplier
}

// ChooseNonce searches a nonce such that block header is valid.
func (cs *Celtus) ChooseNonce() error {
	for cs.Block.Nonce = 0x0; cs.Block.Nonce <= 0xffff; cs.Block.Nonce++ {
		header := hashToBig(cs.Block.BlockHash())
		if headerHashMinimum.Cmp(header) < 0 {
			return nil
		}
	}
	return fmt.Errorf("could not find a suitable nonce")
}

// PrepareSearch sets up search.
func (cs *Celtus) PrepareSearch() {
	cs.fixedMultiplier = hashToBig(cs.Block.BlockHash())
	// Multiply fixedMultiplier by a small number to eliminate small factors.
	cs.coprimeMultiplier = computeCoprimeMultiplier(cs.fixedMultiplier, *coprimeFactors)
	// Fixed multiplier is given by the block's hash times the coprimeMultiplier.
	cs.fixedMultiplier = big.NewInt(1).Mul(cs.fixedMultiplier, cs.coprimeMultiplier)
	cs.modulos = GetNonZeroMod(cs.fixedMultiplier, SmallPrimes)
	cs.chainMultiplier = 0
	cs.chainMultiplierLimit = 1
	cs.origin = big.NewInt(0) // chainMultiplier is 0, so origin is 0.
}

func (cs *Celtus) buildPrimecoinShare(type_ ChainType, length uint, shift uint) *PrimecoinHeader {
	multiplier := big.NewInt(cs.chainMultiplier)
	multiplier.Mul(multiplier, cs.coprimeMultiplier)
	multiplier.Lsh(multiplier, shift)
	// log.Println("Raw fermatStats ", fermatStats)
	log.Printf("Found %v chain of length %d with multiplier 2**%d * %d * %v = %v",
		type_, length, shift, cs.chainMultiplier, cs.coprimeMultiplier, multiplier)

	return &PrimecoinHeader{
		BlockHeader:     cs.Block,
		ChainType:       type_,
		ChainLength:     int(length),
		ChainMultiplier: multiplier,
	}
}

// SearchChainMultiplier searches a chain multiplier and returns a
// primecoin share with a chain of at least chainLength has been found.
// If share cannot be found, it will return nil.
//
// PrepareSearch must be called before SearchChainMultiplier.
func (cs *Celtus) SearchChainMultiplier(minChainLength uint, done <-chan struct{}) *PrimecoinHeader {
	for chainMultiplier := 1 + cs.chainMultiplier; ; chainMultiplier++ {
		// Check if the computation has ended.
		select {
		case <-done:
			cs.chainMultiplier = chainMultiplier
			return nil
		default:
		}

		if chainMultiplier == math.MaxInt64 {
			// TODO: Handle overflows, don't panic.
			panic("chainMultiplier about to overflow")
		}

		// If the current range was finished, advance to the next one.
		if chainMultiplier == cs.chainMultiplierLimit {
			start, limit := cs.chainMultiplierLimit, cs.chainMultiplierLimit+int64(*sieveSize)
			cs.chainMultiplierLimit = limit
			cs.sieve1 = NewSieve(cs.modulos, start, limit, -1)
			cs.sieve2 = NewSieve(cs.modulos, start, limit, +1)
		}

		cs.origin.Add(cs.origin, cs.fixedMultiplier)
		if chainMultiplier%2 == 0 {
			// Skip even chainMultiplier because the chain is already
			// tested at chainMultiplier / 2.
			continue
		}

		cc1PrimeTester := newPrimeTester(cs.origin, ChainCunningham1, cs.sieve1.ChainPrimeBitmap(chainMultiplier))
		cc2PrimeTester := newPrimeTester(cs.origin, ChainCunningham2, cs.sieve2.ChainPrimeBitmap(chainMultiplier))

		btwChainLength, btwShift := findBitwinChain(minChainLength, cc1PrimeTester, cc2PrimeTester)
		if btwChainLength >= minChainLength {
			numShift.Add(fmt.Sprint(btwShift), 1)
			numBTW.Add(fmt.Sprint(btwChainLength), 1)
			cs.chainMultiplier = chainMultiplier
			return cs.buildPrimecoinShare(ChainBitwin, btwChainLength, btwShift)
		}

		cc1ChainLength, cc1Shift := findCunninghamChain(minChainLength, cc1PrimeTester)
		if cc1ChainLength >= minChainLength {
			numShift.Add(fmt.Sprint(cc1Shift), 1)
			numCC1.Add(fmt.Sprint(cc1ChainLength), 1)
			cs.chainMultiplier = chainMultiplier
			return cs.buildPrimecoinShare(ChainCunningham1, cc1ChainLength, cc1Shift)
		}

		cc2ChainLength, cc2Shift := findCunninghamChain(minChainLength, cc2PrimeTester)
		if cc2ChainLength >= minChainLength {
			numShift.Add(fmt.Sprint(cc2Shift), 1)
			numCC2.Add(fmt.Sprint(cc2ChainLength), 1)
			cs.chainMultiplier = chainMultiplier
			return cs.buildPrimecoinShare(ChainCunningham2, cc2ChainLength, cc2Shift)
		}
	}
}
