package main

import (
	"math/big"
	"testing"
)

var (
	largePrimes = []int64{289327979, 1123465789, 1234567891, 13666666666666613}

	bigPrimes = []string{
		"100504109968880373827186665680588058462440135804230652138761525509616868569959110034130581019688959",
		"12374422883751689078591501566018599380884707355136098640207519881003259915425200666099320494079",
		"13037779456932560919702195120170722758017962268955769739254857701826930535356748349368973260799",
		"1368613993736865241334097139982738589923198136690451742249926368361602349499729030043705143041",
		"1682385181343828158731801524573044820303914589942655212903035494514500806294478379696651272191999",
		"1688538420460775357345106776645738286782255672458998497364939106750008338907889379279198165328199681",
		"197426207354341176882050668839997910624254675202331049648386413567691587906435936033529848954879",
		"199084157725742039415784111040834234942501952609942847295539966006837982760373893308403934561",
	}
)

func testPrimality(t *testing.T, p int64, prime bool) {
	fermat := p == 2 || FermatPseudoPrime(big.NewInt(p), plusTwo)
	if fermat != prime {
		if prime {
			t.Error("Expected ", p, " to be prime, got composite")
		} else {
			t.Error("Expected ", p, " to be composite, got prime")
		}
	}
}

func TestFermatPrime(t *testing.T) {
	for _, p := range SmallPrimes {
		testPrimality(t, p, true)
	}
	for _, p := range largePrimes {
		testPrimality(t, p, true)
	}
}

func TestFermatComposite(t *testing.T) {
	for _, p := range []int64{2 * 3, 16 * 81, 25 * 49 * 121} {
		testPrimality(t, p, false)
	}
}

func TestFermatCarmichael(t *testing.T) {
	for _, p := range []int64{561, 1105, 1729, 2465, 2821, 6601, 8911} {
		testPrimality(t, p, true)
	}
}

func hasDivisor(v *big.Int, primes []int64) bool {
	for _, p := range primes {
		if BigMod(v, p) == 0 {
			return true
		}
	}
	return false
}

// Test sieve for n.
func testSieveN(t *testing.T, n *big.Int) {
	mod := GetNonZeroMod(n, SmallPrimes[:1024])

	for _, delta := range []int64{-1, +1} {
		start, limit := int64(1), int64(200)
		s := NewSieve(mod, start, limit, delta)
		d := big.NewInt(delta)

		for m := start; m < limit; m++ {
			if m%2 == 0 {
				continue
			}

			// v is the chain origin tested:
			v := big.NewInt(m)
			v.Mul(v, n)

			bitmap := s.ChainPrimeBitmap(m)
			for i := uint(0); i < 32; i++ {
				v.Lsh(v, i)
				v.Add(v, d)

				bit := uint32(1) << i
				has := hasDivisor(v, SmallPrimes[:1024])

				if bitmap&bit != 0 && has {
					t.Errorf("has divisor, expected possible prime")
				}
				if bitmap&bit == 0 && !has {
					t.Errorf("no divisor, expected composite")
				}

				v.Sub(v, d)
				v.Rsh(v, i)
			}
		}
	}
}

func TestSieve(t *testing.T) {
	n := new(big.Int)
	n.SetString("89012302118294945913527852560669227610448148164408794304935511321715883742280", 10)
	testSieveN(t, n)
	n.SetString("266440233446881470326851433575616264850355126219625529176708600109237757740736626179796706", 10)
	testSieveN(t, n)
	n.SetString("1608068393321310045967547065656817224688088320729311340834943167508011318520433053980571740", 10)
	testSieveN(t, n)
}

func BenchmarkFermatPrimalityTest(b *testing.B) {
	for i := 0; i < b.N; i++ {
		for _, p := range bigPrimes {
			bi, _ := new(big.Int).SetString(p, 10)
			FermatPseudoPrime(bi, plusTwo)
		}
	}
}

func BenchmarkRabinMillerPrimalityTest(b *testing.B) {
	for i := 0; i < b.N; i++ {
		for _, p := range bigPrimes {
			bi, _ := new(big.Int).SetString(p, 10)
			bi.ProbablyPrime(32)
		}
	}
}
