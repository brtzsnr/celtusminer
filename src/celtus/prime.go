package main

import (
	"expvar"
	"flag"
	"fmt"
	"math/big"
)

var (
	maxSmallPrime = flag.Int64("max_small_prime", int64(1<<22), "maximum small prime. Used mainly for sieving")

	numComposite    = expvar.NewInt("NumComposite")
	numPseudoPrimes = expvar.NewInt("NumPseudoPrimes")

	// All primes up to maxSmallPrime
	SmallPrimes []int64
)

// BigMod computes modulo between a big number and an int.
func BigMod(b *big.Int, mod int64) int64 {
	return new(big.Int).Mod(b, big.NewInt(mod)).Int64()
}

// FermatPseudoPrime performes Fermat pseudo primality test.
// http://en.wikipedia.org/wiki/Fermat_primality_test
// Fails for base and Carmichael numbers.
func FermatPseudoPrime(prime *big.Int, base *big.Int) bool {
	pseudoPrime := new(big.Int).Exp(base, prime, prime).Cmp(base) == 0
	if pseudoPrime {
		numPseudoPrimes.Add(1)
	} else {
		numComposite.Add(1)
	}
	return pseudoPrime
}

// StrongPseudoPrime performs primality test for prime using more
// complex and more expensive algorithms.
func StrongPseudoPrime(prime *big.Int) error {
	for _, b := range []int64{3, 5, 7, 11, 13, 17, 19, 23, 29, 31} {
		if !FermatPseudoPrime(prime, big.NewInt(b)) {
			return fmt.Errorf("input failed FermatPseudoPrime with base %d", b)
		}
	}
	if !prime.ProbablyPrime(50) {
		return fmt.Errorf("input failed Miller-Rabin")
	}
	return nil
}

// Factorizes n. Uses a O(n^0.5) algorithm, useful for testing.
func Factorize(n int64) []int64 {
	factors := make([]int64, 0)
	for i := int64(2); i*i <= n && n > 1; i++ {
		for n%i == 0 {
			factors = append(factors, i)
			n /= i
		}
	}

	if n != 1 {
		factors = append(factors, n)
	}
	return factors
}

// egcd implements extended Euclid algorithm.
// Return (g, x, y) such that a*x + b*y = g = gcd(a, b).
// http://en.wikibooks.org/wiki/Algorithm_Implementation/Mathematics/Extended_Euclidean_algorithm
func egcd(a, b int64) (int64, int64, int64) {
	x, y := int64(0), int64(1)
	u, v := int64(1), int64(0)
	for a != 0 {
		q, r := b/a, b%a
		m, n := x-u*q, y-v*q
		b, a, x, y, u, v = a, r, u, v, m, n
	}
	return b, x, y
}

// modinv computes modular inverse of a modulo m.
// Returns -1 if gcd(a, b) != 1 (i.e no modular inverse).
// http://en.wikibooks.org/wiki/Algorithm_Implementation/Mathematics/Extended_Euclidean_algorithm
func modinv(a, m int64) int64 {
	g, x, _ := egcd(a, m)
	if g == 1 {
		return (x + m) % m
	}
	return -1
}

type Modulo struct {
	Remainder, Divisor             int64 // remainder, divisor
	InverseOfRemainder, InverseOf2 int64 // inverse of remainder, inverse of 2 (modulo divisor)
}

// getNonZeroMod returns a vector of pairs of (modulo, n % divisor)
// if n % quotent != 0 and quotent from q.
func GetNonZeroMod(n *big.Int, q []int64) []Modulo {
	mod := make([]Modulo, 0)
	for _, divisor := range q {
		remainder := BigMod(n, divisor)
		if remainder != 0 {
			mod = append(mod, Modulo{
				Remainder:          remainder,
				Divisor:            divisor,
				InverseOfRemainder: modinv(remainder, divisor),
				InverseOf2:         modinv(2, divisor),
			})
		}
	}
	return mod
}

// Sieve is an Erathostene sieve for odd chain multipliers.
type Sieve struct {
	start, limit int64
	// Bit representation of primeBitmap is
	// 0 if possible prime, 1 if definetely composite.
	primeBitmap []uint32
}

// NewSierve builds a new sieve for [start, limit) range of integers
// to test Cunningham chains multipliers. Cunningham chain type (1 or 2) is given by delta.
// Expects is that modulo[i].Divisor is prime and greater than 2.
// Only odd multipliers are tested.
func NewSieve(mod []Modulo, start, limit, delta int64) *Sieve {
	if start%2 == 0 {
		start++
	}
	if limit%2 == 0 {
		limit++
	}

	s := &Sieve{
		start:       start,
		limit:       limit,
		primeBitmap: make([]uint32, (limit-start)/2),
	}

	for i := range mod {
		// r == fixed multiplier (mod d)
		r, d := mod[i].Remainder, mod[i].Divisor
		if r == 0 {
			continue
		}
		if d == 2 {
			panic("Divisor == 2")
		}

		// inv == r^-1 * delta (mod d)
		inv := (mod[i].InverseOfRemainder * (d - delta)) % d
		for length := uint(0); length < 32; length++ {
			// n is the chain multiplier
			// if n == inv (mod d) then n * fixed_multiplier + delta
			// is divisible by d so the chain can not be longer than length

			// Set n to the smallest number larger than or equal to start
			// such that n == inv (mod d)
			n, m := start, start%d
			if m <= inv {
				n += inv - m
			} else {
				n += d - m + inv
			}

			if n%2 == 0 {
				// Only odd chain multipliers are tested.
				// If n is even, jump to next composite multiplier.
				n += d
			}

			// Test only multipliers such that n == inv (mod d).
			// Since only odd chain multipliers are tested and
			// n is odd (see above) skip every other chain multiplier.
			mask := uint32(1 << length)
			for ; n < limit; n += 2 * d {
				s.primeBitmap[(n-start)/2] |= mask
			}

			inv = (inv * mod[i].InverseOf2) % d
		}
	}

	return s
}

// ChainPrimeBitmap returns a bitmap identifying possible primes in a Cunningham Chain.
// bit i is set if fixed_multiplier * n * 2^i + delta is possible prime.
// and cleared if it is known to be composite.
func (s *Sieve) ChainPrimeBitmap(n int64) uint32 {
	if n%2 == 0 {
		panic("expected odd chain multiplier")
	}
	return ^s.primeBitmap[(n-s.start)/2]
}

// GenerateSmallPrimes finds all primes up to max.
// TODO: This O(max^1.5) which is slow given that max ~= 2^21.
// Normally runs once, at the start of the program, but it slows down tests.
func GenerateSmallPrimes(max int64) []int64 {
	primes := make([]int64, 0)
	if max < 2 {
		return primes
	}

	primes = append(primes, 2)
	for prime := int64(3); prime <= max; prime += 2 {
		composite := false
		for div := int64(3); div*div <= prime; div += 2 {
			if prime%div == 0 {
				composite = true
				break
			}
		}
		if !composite {
			primes = append(primes, prime)
		}
	}

	return primes
}

func init() {
	SmallPrimes = GenerateSmallPrimes(*maxSmallPrime)
}
